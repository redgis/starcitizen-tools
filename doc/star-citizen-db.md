# Star Citizen Database

This database is in no way exaustive. It's not a Wiki. 

It only and strictly aimes at containing technical data useful for the various tools.

For now it contains the following information :
- System names
- Planets and moons names and sizes, and OM altitudes
- Locations, with assotiated OM/center distances if needed (needed for hidden places)
- Goods that can be traded
- Prices of goods in each location