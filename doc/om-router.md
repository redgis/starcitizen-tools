# OM Router

This simple tool helps you find your way to hidden locations in Star Citizen universe. Indeed, this script allows one to find which waypoints to follow to get to an unmarked POI on a planet. e.g Jumptown, The Orphanage, Javelin shipwreck.

Since we currently don't have any in-game tool to bookmark a location and/or share it with other players, all we can do is use visual landmarks if we're lucky, or if one has distances to OMs, one can spend 45 minutes doing manual triangulation to adjuste bit by bit the distance to those OMs.

We can make this triangulation faster and reliable using a (very small) bit of math (i.e. OM points).

The method I propose here is :
- Find closest OM. Go to OM, stop.
- Find second closest OM, fly in direction of this OM in straight line, for N km (calculated by this program)
  will calculate for you), stop.
- Find third closest OM, fly in direction of this OM for M km (calculated by this programm), stop.
- Point to center of planet, fly in that direction until you reach ground.
- You reached your destination (suposedly).

If you are going to an hidden outpost, it is recommented to go there by night to spot it more easily with its lights.

If you are going to a ship wreck, daylight will probably help you find it ...


## How to use
--------
This tool is a simple [Python 3](https://www.python.org/downloads/) script. 

### **Prerequisites**

To run it, you need
- [Python 3](https://www.python.org/downloads/)
- numpy package
- [Star Citizen Database](../starcitizen.sqlite3) in same folder as the script

Make sure you have python.exe and pip.exe in your path (in Python install directory, and script/ subfolder).

For isntance if python is installed in d:\Python-3.7, you may want to open a command line and run :

> `set PATH=d:\Python-3.7\Scripts;d:\Python-3.7;%path%`

Then :

> `pip install numpy`

### **Usage**

> `python "OM route.py"`

```
$ python "OM route.py"
Select location number you want to go to :
0 -   ('Jumptown', 'Yela', 492.0, None, 377.6, 570.6, 597.0, None, 324.0, 313.0)
1 -   ('The Orphanage', 'Lyria', 351.7, 201.4, 553.7, 331.9, 486.8, 477.5, 345.1, 223.3)
2 -   ('Javelin shipwreck', 'Daymar', 463.5, 586.4, 512.4, 222.7, 746.3, 454.5, 632.4, 295.2)
2


Here is your trip :

1) Goto OM3
2) Point to OM5, fly straight for : 178.76945077088988 km
3) Point to OM2, fly straight for : 59.99666029050193 km
4) Point to planet center, target is around on the surface.
```

To add your own locations, edit the data base (for instance using https://sqlitebrowser.org/).

## Technical details.
--------

Here are details about how this tools works.

All this calculation is based on the fact that OMs are placed in very particulare spots around the planet.

![OMs](om-router-01.jpg "OM locations")

OM1 and OM2 are the north and south poles, the rest are on the equatorial plane. All the OMs are at same distance from the planet center.

Let's focus on a particular side. Say we have a point on the planet, and we can determine it's distances to each OM, and to the planet center.

![OMs](om-router-02.jpg "Point on planet")

Remember, all we can use in Star Citizen, currently, are QT markers : OMs and planet center marker.

In this example, the idea is to find which point P' in the plane (OM1,OM3,OM6) is aligned with our destination point P, and the planet center, so that once we reach this point, all we have to do is aim at the planet center and reach ground.

![OMs](om-router-03.jpg "Point on plane")

Once we have found this point we can find the route R1 and R2 that needs to be followed.

![OMs](om-router-04.jpg "Routes to P'")

Illustration made using [GeoGebra](https://www.geogebra.org/classic)