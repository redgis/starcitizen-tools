# This script allows one to find waypoints to follow to get to an unmarked POI on a
# planet. e.g Jumptown, The Orphanage, Javelin shipwreck.

# Since we currently don't have the right tools to bookmark a location, all we can do is use visual
# makrs if there are any, if we're lucky. Or we can use the existing reliable reference points to 
# do triangulation (i.e. OM points).

# The method I propose here is :
# - Find closest OM. Go to OM, stop.
# - Find second closest OM, fly in direction of this OM in straight line, for N km (calculated by this program)
#   will calculate for you), stop.
# - Find third closest OM, fly in direction of this OM for M km (calculated by this programm), stop.
# - Point to center of planet, fly in that direction until you reach ground.
# - You reached your destination (suposedly)
#
# This script uses the following data as input for route calculation :
# - planet radius
# - distance from POI to each 3 closest OMs (you may as well take note of all the OMs, the programm will pick the right ones for you)
# - OM radius : the distance of the OMs to the center of planet

import math
import numpy as np
import collections
import sqlite3

# import localization as lx

# Calculate height of a triangle using Heron formula (see https://www.ilemaths.net/sujet-formule-pour-calculer-la-hauteur-d-un-triangle-quelconque-310236.html,  https://en.wikipedia.org/wiki/Heron%27s_formula)
def triangleHeight(a, b, c):
   p = (a+b+c)/2.0
   return 2.0 * math.sqrt( p*(p-a)*(p-b)*(p-c)  )  / a




sqldb = sqlite3.connect('starcitizen.sqlite3')

print('Select location number you want to go to : ')

locations = list(sqldb.execute('''
   SELECT 
   location.name,corps.name,
   corps.om_radius,
   location.OM1,location.OM2,location.OM3,location.OM4,location.OM5,location.OM6,location.center
   FROM Location,Corps 
   WHERE corps.id=location.planet'''))

for idx,row in enumerate(locations):
        print(idx, '-  ', row)

selection = int(input(''))
location = list(locations)[selection]

sqldb.close()

# Distance from OMs to planet center
OM_radius = location[2]

OM_dists = {
   'OM1': location[3],
   'OM2': location[4],
   'OM3': location[5],
   'OM4': location[6],
   'OM5': location[7],
   'OM6': location[8]
   }
Center_dist = location[9]

for key,value in OM_dists.items():
   if value == None:
      OM_dists[key] = 999999999

# Unsued : distance between to neighbour OMs
# OM_distance = math.sqrt( OM_radius*OM_radius * 2)

# Arbitrarily decide on which axis each OM is located.
OM_pos = {
   #OM1 and 2 are Y axis (north and south poles)
   'OM1': np.array([[ 0,  1,  0]]),
   'OM2': np.array([[ 0, -1,  0]]),
   #OM3 and 4 are Z axis
   'OM3': np.array([[ 0,  0,  1]]),
   'OM4': np.array([[ 0,  0, -1]]),
   #OM5 and 6 are X axis
   'OM5': np.array([[-1,  0,  0]]),
   'OM6': np.array([[ 1,  0,  0]])
}

# Order OMs from closest to furthest
closestOMs = list( collections.OrderedDict(sorted(OM_dists.items(), key=lambda kv: kv[1])).items() )

# Find which OM is the closest for each axis (X, Y, Z)
if OM_dists['OM1'] < OM_dists['OM2']:
   OMy = 'OM1'
else:
   OMy = 'OM2'

if OM_dists['OM3'] < OM_dists['OM4']:
   OMz = 'OM3'
else:
   OMz = 'OM4'

if OM_dists['OM5'] < OM_dists['OM6']:
   OMx = 'OM5'
else:
   OMx = 'OM6'


# Find height of the triangle (O,P,OMx) (O=origin, P=POI, OMx = closest OM fo X axis) ...
# ... which then allows us to calculate the x coordinate of P
H1 = triangleHeight(OM_radius, Center_dist, OM_dists[OMx])
Px = math.sqrt(Center_dist*Center_dist - H1*H1) * OM_pos[OMx][0][0]

# Same for y
H2 = triangleHeight(OM_radius, Center_dist, OM_dists[OMy])
Py = math.sqrt(Center_dist*Center_dist - H2*H2) * OM_pos[OMy][0][1]

# Same fo z
H3 = triangleHeight(OM_radius, Center_dist, OM_dists[OMz])
Pz = math.sqrt(Center_dist*Center_dist - H3*H3) * OM_pos[OMz][0][2]

# We have P coordinates !
P = np.array([[Px, Py, Pz]])

# # Tried geolocalization package "localization", got very close results.
# # Might be usefull for more generic results
# # See https://pypi.org/project/Localization/
# # The three closest OMs (they are the same as ABC, but not in XYZ order)
# OM_1 = OM_pos[ closestOMs[0][0] ] * OM_radius
# OM_2 = OM_pos[ closestOMs[1][0] ] * OM_radius
# OM_3 = OM_pos[ closestOMs[2][0] ] * OM_radius
#
# lxProject = lx.Project(mode='3D',solver='LSE')
# lxProject.add_anchor('center', (0,0,0))
# lxProject.add_anchor(closestOMs[0][0], (OM_1[0][0], OM_1[0][1], OM_1[0][2]))
# lxProject.add_anchor(closestOMs[1][0], (OM_2[0][0], OM_2[0][1], OM_2[0][2]))
# lxProject.add_anchor(closestOMs[2][0], (OM_3[0][0], OM_3[0][1], OM_3[0][2]))
#
# POI,label=lxProject.add_target()
# POI.add_measure('center', Center_dist)
# POI.add_measure(closestOMs[0][0], closestOMs[0][1])
# POI.add_measure(closestOMs[1][0], closestOMs[1][1])
# POI.add_measure(closestOMs[2][0], closestOMs[2][1])
#
# lxProject.solve()
#
# print('P : ', P)
# print('P : ', POI.loc)


# Just for redability, name our 3 closest OMs A (x axis), B (y axis), and C (z axis)
A = OM_pos[OMx] * OM_radius
B = OM_pos[OMy] * OM_radius
C = OM_pos[OMz] * OM_radius

# We now want to calculate where P is located in the (ABC) plane, and its "normal" vector
AB = B-A
AC = C-A
normal = np.cross(AB, AC)
normal = normal / np.linalg.norm(normal)

# This plane cartesian equation defined using any point M (x, y, z) so that AM is
#  orthogonal to 'normal' : AM . normal = 0
# (see https://homeomath2.imingo.net/planequ.htm)
# i.e.            (M-A) . normal = 0
#    <=> M . normal - A . normal = 0 (dot product properties : https://en.wikipedia.org/wiki/Dot_product)

# From the plane equation, we find k so that k*OP (i.e. k*P) satisfies this equation :
#       (k*P) . normal - A . normal = 0
# <=> k * (P . normal) - A . normal = 0  (dot product properties : https://en.wikipedia.org/wiki/Dot_product)
# <=>              k * (P . normal) = A . normal
# <=>                             k = A . normal / P . normal
k = np.dot(A, np.transpose(normal)) / np.dot(P, np.transpose(normal))

# Projection of P from O on plane (ABC) is as follows. 
ProjectedP = k*P

# This is the point we want to get to : from this point, we simply have to point to the center 
# of the planet (O) and the POI should be there... But, how to get this magic point ?
# Well, lets find the point between the two closest OMs, from which we can point to the third
# closest OM, to go through ProjectedP.

# Take the projection of P on the plane formed by O and the 2 closest OMs. Call this new point N.
# We are looking for the projection of N from O on this (ABC). This is the intermediate waypoint !

# Initialize N to 1 everywhere
N = np.array([[0, 0, 0]])

# Find N coordinates depending on the 2 closest OMs...
if closestOMs[0][0] == OMx:
   N[0][0] = Px
elif closestOMs[0][0] == OMy:
   N[0][1] = Py
elif closestOMs[0][0] == OMz:
   N[0][2] = Pz

if closestOMs[1][0] == OMx:
   N[0][0] = Px
elif closestOMs[1][0] == OMy:
   N[0][1] = Py
elif closestOMs[1][0] == OMz:
   N[0][2] = Pz


# Find projection of N on plan (ABC) same as we did for P
l = np.dot(A, np.transpose(normal)) / np.dot(N, np.transpose(normal))
ProjectedN = l * N

# We now have our waypoints !

# Goto 1st closest OM, fly to 2nd closest OM for [vector norm]km
dist1 = np.linalg.norm(ProjectedN - OM_pos[closestOMs[0][0]]*OM_radius)
print('\n\nHere is your trip : \n')
print ('1) Go to ' + location[1] + ', go to ' + closestOMs[0][0])
print ('2) Point to ' + closestOMs[1][0] + ', fly straight for : ' + str(dist1) + ' km')

# then point to 3rd closest OM, fly for [vector norm]km
dist2 = np.linalg.norm(ProjectedP - ProjectedN)
print ('3) Point to ' + closestOMs[2][0] + ', fly straight for : ' + str(dist2) + ' km')

# then point to planet center.
print ("4) Point to planet center, target is around on the surface.")

