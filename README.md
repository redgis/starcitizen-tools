# Redgis's Star Citizen tools

Here I share some tools I use to help me play Star Citizen.

I share these tools for them to benefit everyone. Feel free to use them, modify them, make improvement suggestions, or even develop tools yourself and share them here.

I share them under GPL3.

- [Star Citizen Database](doc/star-citizen-db.md) Technical data about the world : Planet sizes, POI locations, trading data
- [OM Router](doc/om-router.md) : Find route to location only given it's distance to local OM points and planet center

